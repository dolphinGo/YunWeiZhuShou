// pages/mine/index/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
		btnName: '注销登录',
		account: '',
		name: '',
		about: '欢迎使用运维助手。',
		imgSrc: '/images/head-img.png',
    autoVoice: false,
  },
  autoVoice: function (e) {
    var autoVoice = e.detail.value;
    this.setData({
      autoVoice: autoVoice
    });
    wx.setStorageSync('autoVoice', autoVoice);
  },
	preview: function () {
		var imgSrc = this.data.imgSrc;
		var urls = [];
		urls.push(imgSrc);
		wx.previewImage({
			urls: urls, // 需要预览的图片http链接列表
			success: function () {
				console.log("成功")
			},
			fail: function () {
				console.log("失败")
			}
		})
	},
	modifyPassword: function() {
		wx.navigateTo({
			url: '/pages/sys/modifyPassword/modifyPassword'
		})
	},
	modifyAccount: function() {
		wx.navigateTo({
			url: '/pages/sys/modifyAccount/modifyAccount'
		})
	},
	about: function () {
		wx.showModal({
			title: '关于我们',
			content: this.data.about,
			showCancel: false,
			success: function (res) {
				if (res.confirm) {
					// console.log('用户点击确定')
				}
			}
		})
	},
	logout: function () {
		var that = this;
		wx.showModal({
			title: '提示',
			content: '确定注销登录吗?',
			success: function (res) {
				if (res.confirm) {
					try {
						// wx.removeStorageSync('password');
						wx.removeStorageSync('autoLogin');
						wx.showLoading({
							title: "正在注销",
							mask: true
						})
						that.setData({
							btnName: "正在注销..."
						})
						setTimeout(() => {
							wx.hideLoading();
							that.setData({
								btnName: "注销登录"
							})
							wx.navigateBack({
								delta: 10
							}) 
							// wx.reLaunch({
							// 	url: '/pages/sys/login/login?logout=1'
							// })
						}, 1000)

					} catch (e) {
					}
				} else if (res.cancel) {
				}
			}
		})
	},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

		
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
		try {
			var name = wx.getStorageSync('name');
			var account = wx.getStorageSync('phone');
      var autoVoice = wx.getStorageSync('autoVoice');
			this.setData({
				account: account,
				name: name,
        autoVoice: autoVoice
			})
		} catch (e) {

		}
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
		var that = this;
		try {
			app.getUserInfo(function (userInfo) {
				console.log(userInfo)
				// 头像
				var imgSrc = userInfo.avatarUrl;
				that.setData({
					imgSrc: imgSrc
				})
			})
		} catch (e) {
			console.log("头像获取失败")
		}
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
		return {
			path: '/pages/sys/login/login',
			success: function (res) {
				// 转发成功
			},
			fail: function (res) {
				// 转发失败
			}
		}
  }
})