// pages/operation/index/index.js

var Zan = require('../../../dist/index');
var app = getApp();
var requestUrl = app.globalData.requestUrl;
const md5 = require('../../../utils/md5.js');
const Util = require('../../../utils/util.js');
Page(Object.assign({}, Zan.Field, Zan.TopTips, Zan.Tab, {

  /**
   * 页面的初始数据
   */
  data: {
		loading: false, // 加载中
		nodata: false, // 暂无数据
		nodata_str: '暂无运维消息',
		nomore: false, // 没有更多数据
		feedMsg: [],
		isVerify: false, // 默认无审核权限, 看不到审核按钮
		userid: ""
  },
	handleZanTabChange(e) {
		var componentId = e.componentId;
		var selectedId = e.selectedId;

		this.setData({
			[`${componentId}.selectedId`]: selectedId
		});
	},
	getMsg: function () {
		var that = this;
		try {
			var userid = wx.getStorageSync('userid');
			var sibrccode = wx.getStorageSync('sibrccode');
			console.log("userid: "+ userid + ", sibrcccode: " + sibrccode)
			if (userid && sibrccode) {
				console.log("发送请求")
				wx.request({
					url: requestUrl + "mui/appops",
					method: 'GET',
					dataType: 'json',
					data: {
						userid: userid,
						sibrccode: sibrccode
					},
					header: {
						'content-type': 'application/json'
					},
					success: function (res) {
						var data = res.data;
						console.log(data)
						wx.stopPullDownRefresh();
						that.saveMsg(data);
						if (data.length == 0) {
							that.setData({
								nodata: true
							});
						} else {
							that.setData({
								nodata: false
							});
						}
					},
					fail: function () {
						that.showZanTopTips("消息获取失败, 请重试! ");
						wx.stopPullDownRefresh();
					}
				})
			}
		} catch (e) {
			wx.stopPullDownRefresh();
			wx.showModal({
				title: '提示',
				content: '获取用户信息失败, 请重新拉取或重新登录',
				showCancel: false,
				success: function (res) {
					if (res.confirm) {
						console.log('用户点击确定')
					} else if (res.cancel) {
						console.log('用户点击取消')
					}
				}
			})
		}
	},
	saveMsg: function (data=[]) { // 保存信息
		var feedMsg = [];
		var nday = Util.getDate(0); // 今天
		var yday = Util.getDate(-1); // 昨天
		var bday = Util.getDate(-2); // 前天
		try {
			for (let i = 0; i < data.length; i++) {
				var date = "";
				var time = "";
				if (data[i].dealtime) {
					date = data[i].dealtime;
				} else {
					date = data[i].feedbacktime;
				}
				var nday = Util.getDate(0); // 今天
				var yday = Util.getDate(-1); // 昨天
				var bday = Util.getDate(-2); // 前天
				if (date.substr(0, 10) == nday) { // 今天
					time = "今天" + date.substr(11, 5);
				} else if (date.substr(0, 10) == yday) { // 昨天
					time = "昨天" + date.substr(11, 5);
				} else if (date.substr(0, 10) == bday) { //前天
					time = "前天" + date.substr(11, 5);
				} else { // 显示日期
					time = date.substr(5, 11);
				}

				// // 

				var msg = {
					feedId: data[i].id, // 消息id
					content: data[i].content, //反馈内容
					dealtime: data[i].dealtime, // 处理时间
					feedType: data[i].category, // 反馈类型
					dealtype: data[i].dealtype, // 处理状态 1-待处理 2-处理中 3-已处理 4-退回
					feedMsg: (data[i].replycontent == null ? "" : data[i].replycontent), // 答复信息
					dealuser: data[i].dealuser, // 处理人
					feedbackTime: data[i].feedbacktime,
					attachment: data[i].attachment, // 图片路径
					summary: data[i].content.length > 16 ? (data[i].content.substr(0, 16) + "...") : data[i].content,
					time: time,
					tcUserid: data[i].userid, // 提出人userid
					maUserid: data[i].CauditID, // 市县审核人userid
					paUserid: data[i].PauditID, // 省级审核人userid
					auditlink: data[i].auditlink == null ? '4' : data[i].auditlink,
					auditstat: data[i].auditstat == null ? '4' : data[i].auditstat,
					ma: data[i].CauditName == null ? "" : data[i].CauditName,
					pa: data[i].PauditName == null ? "" : data[i].PauditName,
					presented: data[i].sibrcname + " "+ data[i].Uname,
					maTel: data[i].Cphone == null ? "" : data[i].Cphone,
					paTel: data[i].Pphone == null ? "" : data[i].Pphone
				};
				feedMsg.push(msg);
			}
			this.setData({
				feedMsg: feedMsg
			});
			wx.setStorage({
				key: "feedMsg",
				data: feedMsg
			})
		} catch (e) {
			this.setData({
				feedMsg: feedMsg
			});
		}
		
	},
	gotoFeedDetail: function (event) {
		var feedId = event.currentTarget.dataset.feedid;
		
		wx.navigateTo({
			url: '/pages/operation/feedDetail/feedDetail?id=' + feedId
		})
	},
	addFeed: function () {
		wx.navigateTo({
			url: '/pages/operation/addFeed/addFeed'
		})
	},
	edit: function (event) {
		var id = event.currentTarget.dataset.id;
		console.log(id)
		wx.navigateTo({
			url: '/pages/operation/addFeed/addFeed?id='+id
		})
	}, 
	withdraw: function (event) { // 撤回
		var that = this;

		var id = event.currentTarget.dataset.id;
		console.log(id)
		wx.showModal({
			title: '提示',
			content: '确定撤回此条信息吗?',
			success: function (res) {
				if (res.confirm) {
					console.log('用户点击确定')
					wx.request({
						url: requestUrl + 'mui/withdrawAppops',
						method: 'GET',
						dataType: 'json',
						data: {
							id: id
						},
						header: {
							'content-type': 'application/json'
						},
						success: function (res) {
							var data = res.data;
							console.log(data)
							if (data.result == 'true') {
								wx.startPullDownRefresh();
								that.getMsg();
							}
						},
						fail: function () {

						}
					})
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})
		
	},
	verify: function (event) {
		var id = event.currentTarget.dataset.id;
		console.log(id)
		wx.navigateTo({
			url: '/pages/operation/feedDetail/feedDetail?id=' + id + '&verify=1'
		})
	},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		try {
			// var isVerify = wx.getStorageSync('isVerify')
			var userid = wx.getStorageSync('userid')
			if (userid) {
				this.setData({
					userid: userid
				});
			}
			// if (isVerify == '1') { // 具有审核权限
			// 	this.setData({
			// 		isVerify: true
			// 	});
			// }
		} catch (e) {
			// Do something when catch error
		}
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
		console.log("监听页面初次渲染完成")
		wx.startPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面显示--只要切换到此页面就会执行
   */
  onShow: function () {
		console.log("监听页面显示")
		this.getMsg();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
		console.log("监听页面隐藏")
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
		console.log("监听页面卸载")
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
		this.getMsg();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
		return {
			path: '/pages/sys/login/login',
			success: function (res) {
				// 转发成功
			},
			fail: function (res) {
				// 转发失败
			}
		}
  }
}))