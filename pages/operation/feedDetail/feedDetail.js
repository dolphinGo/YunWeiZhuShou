
// pages/operation/feedDetail/feedDetail.js
var app = getApp();
var Zan = require('../../../dist/index');
var requestUrl = app.globalData.requestUrl;
Page(Object.assign({}, Zan.TopTips,{

  /**
   * 页面的初始数据
   */
  data: {
		cursorSpacing: 50,
		showConfirm: false,
		detailMsg: {},
		imgUrls: [],
		verify: true,
		typeList: [
			{
				value: '2',
				name: '审核通过',
				checked: true
			},
			{
				value: '3',
				name: '审核拒绝'
			}
		],
		verifyType: '2', // 审核是否通过
		content: "审核通过", // 审核内容
		isRadioChange: true,
		id: "",
		steps: [],
		userid: "", // 用户id
		checkFlag: '',
		sibrccode: '',
		checkers: []
  },
	radioChange: function (e) {
		console.log(e)
		var isRadioChange = this.data.isRadioChange;
		
		var verifyType = e.detail.value;
		console.log("选择的是"+verifyType)
		this.setData({
			verifyType: verifyType
		})
		if (isRadioChange) {
			this.setData({
				isRadioChange: false,
				content: ""
			})
		}
	},
	inputContent: function (e) {
		var content = e.detail.value;
		this.setData({
			content: content
		});
	},
  /**
   * 生命周期函数--监听页面加载
   */
	preview: function (event) {
		var current = event.currentTarget.dataset.currentsrc;
		console.log(current)
		var urls = this.data.imgUrls;
		wx.previewImage({
			current: current, // 当前显示图片的http链接
			urls: urls // 需要预览的图片http链接列表
		})
	},
	getCheckers: function () {
		var id = this.data.id;
		var that = this;
		wx.request({
			url: requestUrl + 'mui/getCheckers',
			method: 'GET',
			dataType: 'json',
			data: {
				id: id
			},
			header: {
				'content-type': 'application/json'
			},
			success: function (res) {
				var data = res.data;
				that.setData({
					checkers: data
				});
			},
			fail: function () {

			}
		})
	},
	getPhotos: function () {
		var id = this.data.id;
		var that = this;
		console.log("getPhotos: "+ id)
		wx.request({
			url: requestUrl + 'mui/showPhoto',
			method: 'GET',
			dataType: 'json',
			data: {
				id: id
			},
			header: {
				'content-type': 'application/json'
			},
			success: function (res) {
				var data = res.data;
				console.log(JSON.stringify(data))
				var attachment = data.photourl;
				if (attachment == "") { // 如果图片为空以下代码不执行
					return false;
				}
				// 图片处理
				var path = attachment.split('|');
				var imgUrls = [];
				for (let i = 0; i < path.length; i++) {
					var attachmentPath = requestUrl + path[i];
					imgUrls.push(attachmentPath);
				}
				that.setData({
					imgUrls: imgUrls
				});
				console.log(imgUrls)
			},
			fail: function () {

			}
		})
	},
  onLoad: function (options) {
		var id = options.id;
		
		this.setData({
			id: id
		});
		// var attachment = "";
		try {
			var msgs = wx.getStorageSync('feedMsg')
			var userid = wx.getStorageSync('userid')
			var checkFlag = wx.getStorageSync('checkflag');
			var sibrccode = wx.getStorageSync('sibrccode');
			this.setData({
				userid: userid,
				checkFlag: checkFlag,
				sibrccode: sibrccode
			});
			if (msgs) {
				for (let i = 0; i < msgs.length; i++ ) {
					if (msgs[i].feedId == id) {
						var detailMsg = msgs[i];
						this.setData({
							detailMsg: detailMsg
						});
						break;
					}
				}
				
			}
		} catch (e) {
			// Do something when catch error
		}
  },
	edit: function (event) {
		var id = event.currentTarget.dataset.id;
		console.log(id)
		wx.navigateTo({
			url: '/pages/operation/addFeed/addFeed?id=' + id
		})
	},
	withdraw: function (event) { // 撤回
		var that = this;

		var id = event.currentTarget.dataset.id;
		console.log(id)
		wx.showModal({
			title: '提示',
			content: '确定撤回此条信息吗?',
			success: function (res) {
				if (res.confirm) {
					console.log('用户点击确定')
					wx.request({
						url: requestUrl + 'mui/withdrawAppops',
						method: 'GET',
						dataType: 'json',
						data: {
							id: id
						},
						header: {
							'content-type': 'application/json'
						},
						success: function (res) {
							var data = res.data;
							console.log(data)
							if (data.result == 'true') {
								// wx.startPullDownRefresh();
								// that.getMsg();
								wx.navigateBack({
									delta: 1
								})
							}
						},
						fail: function () {

						}
					})
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})

	},
	gotoVerify: function (event) {
		// var id = event.currentTarget.dataset.id;
		// console.log(id)
		// wx.navigateTo({
		// 	url: '/pages/operation/feedDetail/feedDetail?id=' + id + '&verify=1'
		// })
		this.setData({
			verify: false
		});
		wx.setNavigationBarTitle({
			title: '审核'
		})
	},
	verify: function () { //审核
		var that = this;
		try {
			var userid = this.data.userid;
			if (userid) {
				var id = this.data.id; // 问题id
				var flag = this.data.verifyType; //审核标志 2-审核通过，3-审核拒绝
				var dealDesc = this.data.content;
				if (flag == '3' && dealDesc.trim() == '') { // 如果驳回,必须写理由
					this.showZanTopTips("请填写审批意见/理由");
					return false;
				}
				console.log(id + "," + flag + "," + dealDesc)
				// 发送请求
				wx.showLoading({
					title: '操作中',
					mask: true
				})
				var data = {
					userid: userid,
					id: id,
					flag: flag,
					dealDesc: dealDesc
				}
				wx.request({
					url: requestUrl + "mui/checkAppops",
					data: data,
					success: function (res) {
						var data = res.data;
						console.log("审核成功: " + JSON.stringify(data));
						wx.hideLoading()
						if (data.result == "true") {
							wx.showToast({
								title: '成功',
								icon: 'success',
								duration: 2000
							})
							wx.navigateBack({
								delta: 1
							})
						} else {
							that.showZanTopTips(data.message);
						}
					},
					fail: function () {
						wx.hideLoading()
						var autoLogin = that.data.autoLogin;
						if (autoLogin) {
							that.setData({
								autoLogin: false
							})
							setTimeout(() => {
								that.showZanTopTips("自动登录失败! ");
							}, 300)
						}

					}
				})
			}
			
		} catch (e) {

		}
		
	},
	getLog: function () {
		var id = this.data.id;
		var that = this;
		wx.request({
			url: requestUrl + "mui/appopsLog",
			method: 'GET',
			dataType: 'json',
			data: {
				id: id
			},
			success: function (res) {
				var data = res.data;
				console.log("log: " + JSON.stringify(data))
				that.dealLog(data)
			}
		})
	},
	dealLog: function (data = []) {
		var steps = [];
		for (let i = 0; i < data.length; i++) {
			var step = {
				current: false,
				done: false,
				text: data[i].oprtime,
				content: data[i].opruser+"   "+data[i].oprcontent,
				desc: (data[i].remark == null ? "" : "处理结果: "+data[i].remark)
			}
			steps.push(step);
		}
		// if (data.length == 0) {
		// 	this.setData({
		// 		nodata: true
		// 	});
		// }
		this.setData({
			steps: steps
		});
	},
	call: function (event) {
		var tel = event.currentTarget.dataset.tel;
		wx.makePhoneCall({
			phoneNumber: tel
		})
	},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
		this.getLog();
		this.getCheckers();
		this.getPhotos();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
		return {
			path: '/pages/sys/login/login',
			success: function (res) {
				// 转发成功
			},
			fail: function (res) {
				// 转发失败
			}
		}
  }
}))