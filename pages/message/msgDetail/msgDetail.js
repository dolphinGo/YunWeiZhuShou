// pages/message/msgDetail/msgDetail.js
var wemark = require('../../../wemark/wemark.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
		detailMsg: {},
		content: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		var id = options.id;
		try {
			var msgs = wx.getStorageSync('sysMsg')
			if (msgs) {
				for (let i = 0; i < msgs.length; i++) {
					if (msgs[i].msgId == id) {
						var detailMsg = msgs[i];
						console.log(detailMsg)
						this.setData({
							detailMsg: detailMsg
						});
						wemark.parse(detailMsg.content, this, {
							// 新版小程序可自适应宽高
							imageWidth: wx.getSystemInfoSync().windowWidth - 40,
							name: 'content'
						})
						break;
					}
				}
			}
      var autoVoice = wx.getStorageSync('autoVoice');
      if (autoVoice == true){
        setTimeout(function () {
          this.audioCtx = wx.createAudioContext('myAudio');
          this.audioCtx.play();
        }, 1000)
      }
      
		} catch (e) {
			// Do something when catch error
		}
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 使用 wx.createAudioContext 获取 audio 上下文 context
    // this.audioCtx = wx.createAudioContext('myAudio');
	
  },
  // audioPlay: function () {
  //   this.audioCtx.play()
  // },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
		return {
			path: '/pages/sys/login/login',
			success: function (res) {
				// 转发成功
			},
			fail: function (res) {
				// 转发失败
			}
		}
  }
})