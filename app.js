//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    
  },
	getUserInfo: function (cb) {
		var that = this
		wx.getUserInfo({
			withCredentials: false,
			success: function (res) {
				console.log(res)
				that.globalData.userInfo = res.userInfo
				typeof cb == "function" && cb(that.globalData.userInfo)
			}
		})
	},
  globalData: {
		globalData: null,
		requestUrl: "http://140.143.183.124:80/"
		// requestUrl: "http://192.168.250.100:8090/" // 桃姐
		// requestUrl: "http://192.168.250.15:8090/" //修平
		// requestUrl: "http://192.168.250.102:8090/" // 小宇
  }
})