const isNull = s => {
	if (s.trim() == "") {
		return true;
	} else {
		return false;
	}
}

const isIdNum = s => {
	const reg = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
	if (reg.test(s.trim())) {
		return true;
	} else {
		return false;
	}
}
const isTel = s => {
	const reg = /^(1[3|4|5|7|8][0-9])\d{8}$/;
	if (reg.test(s.trim())) {
		return true;
	} else {
		return false;
	}
}
const getDate = num => {
	var date = new Date();
	date.setDate(date.getDate() + num);
	var month = date.getMonth() + 1;
	var day = date.getDate();
	if (month < 10) {
		month = "0" + month;
	}
	if (day < 10) {
		day = "0" + day;
	}
	return date.getFullYear() + "-" + month + "-" + day;
}
const getTimestamp = function () { // 原为时间戳,后改为随机码
	var len = 16; // 随机串长度
　var $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
　var maxPos = $chars.length;
	var str = "";
　for (let i = 0; i < len; i++) {
		str += $chars.charAt(Math.floor(Math.random() * maxPos));
　}
　return str;
}
const replaceStr = str => {
	
	str = str.replace(/[>]+/g, "");
	str = str.replace(/[*]+/g, "");
	str = str.replace(/[-]+/g, "");
	str = str.replace(/[#]+/g, "");
	str = str.replace(/[~]+/g, "");
	str = str.replace(/[=]+/g, "");
	return str;  
}
module.exports = {
	isNull: isNull,
	isIdNum: isIdNum,
	isTel: isTel,
	getDate: getDate,
	getTimestamp: getTimestamp,
	replaceStr: replaceStr
}
